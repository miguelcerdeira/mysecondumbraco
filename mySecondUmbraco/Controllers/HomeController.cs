﻿using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace mySecondUmbraco.Controllers
{
    public class HomeController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/Home/";

        public ActionResult RenderWork()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Work.cshtml");
        }

        public ActionResult RenderServices()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Services.cshtml");
        }

        public ActionResult RenderContent()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Content.cshtml");
        }

        public ActionResult RenderBlog()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Blog.cshtml");
        }
    }
}